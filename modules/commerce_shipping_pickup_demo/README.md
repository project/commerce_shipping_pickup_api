# Commerce Shipping Pickup Demo

A minimal sample pickup operator module, implementing the
[commerce_shipping_pickup_api](https://www.drupal.org/project/commerce_shipping_pickup_api) framework module.

## Installation

Install and enable the module through Composer:
```
composer require drupal/commerce_shipping_pickup_demo
drush en commerce_shipping_pickup_demo
```

Add the new `pickup_capable_shipping_information` pane with the label _Shipping information_
(look for the one that says _Supports pickup: Yes_ in the summary) to your checkout flow
or use the pre-built `pickup` checkout flow.

Add and set up a new shipping method in your store configuration.