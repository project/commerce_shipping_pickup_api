<?php

namespace Drupal\commerce_shipping_pickup_demo\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\ShippingMethod\PickupShippingMethodBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides a specific pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup_demo",
 *   label = @Translation("Pickup shipping - Demo"),
 * )
 */
class PickupDemoShipping extends PickupShippingMethodBase {
  use StringTranslationTrait;

  /**
   * Some example dealers.
   *
   * @var array
   */
  protected $dealers = [
    [
      'country_code' => "CH",
      'locality' => "Zürich",
      'postal_code' => "8000",
      'address_line1' => "Strasse 1",
      'organization' => "Dealer 1",
    ],
    [
      'country_code' => "CH",
      'locality' => "Bern",
      'postal_code' => "5000",
      'address_line1' => "Strasse 2",
      'organization' => "Dealer 2",
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile): void {
    $id = $profile->getData('pickup_location_data');
    $profile->set('address', $this->dealers[$id]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    return [
      '#type' => 'select',
      '#title' => $this->t('Select a point:'),
      '#default_value' => $profile->getData('pickup_location_data'),
      '#options' => array_map(function ($address) {
        return $address['organization'];
      }, $this->dealers),
    ];
  }
}
