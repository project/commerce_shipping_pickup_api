<?php

namespace Drupal\Tests\commerce_shipping_pickup_api\FunctionalJavascript;

use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Url;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\commerce\FunctionalJavascript\CommerceWebDriverTestBase;

/**
 * Tests the "Shipping information" checkout pane.
 *
 * @group commerce_shipping
 */
class DealerPickupTest extends CommerceWebDriverTestBase {

  /**
   * A French address.
   *
   * @var array
   */
  protected $frenchAddress = [
    'country_code' => 'FR',
    'locality' => 'Paris',
    'postal_code' => '75002',
    'address_line1' => '38 Rue du Sentier',
    'given_name' => 'Leon',
    'family_name' => 'Blum',
  ];

  /**
   * A dealer address.
   *
   * @var array
   */
  protected $dealerAddress = [
    'country_code' => 'CH',
    'locality' => 'Bern',
    'postal_code' => '5000',
    'address_line1' => 'Strasse 2',
    'organization' => 'Dealer 2',
  ];

  /**
   * First sample product.
   *
   * @var \Drupal\commerce_product\Entity\ProductInterface
   */
  protected $product;

  /**
   * A sample order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * The shipping order manager.
   *
   * @var \Drupal\commerce_shipping\ShippingOrderManagerInterface
   */
  protected $shippingOrderManager;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce_payment',
    'commerce_payment_example',
    'commerce_promotion',
    'commerce_tax',
    'commerce_shipping_test',
    'commerce_shipping_pickup_api',
  ];

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions(): array {
    return array_merge([
      'administer commerce_order',
      'administer commerce_shipment',
      'access commerce_order overview',
    ], parent::getAdministratorPermissions());
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->store->set('billing_countries', ['FR', 'US']);
    $this->store->save();

    // Turn off verification via external services.
    $tax_number_field = FieldConfig::loadByName('profile', 'customer', 'tax_number');
    $tax_number_field->setSetting('verify', FALSE);
    $tax_number_field->save();

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::create([
      'id' => 'cod',
      'label' => 'Manual',
      'plugin' => 'manual',
      'configuration' => [
        'display_label' => 'Cash on delivery',
        'instructions' => [
          'value' => 'Sample payment instructions.',
          'format' => 'plain_text',
        ],
      ],
    ]);
    $payment_gateway->save();

    $variation_type = ProductVariationType::load('default');
    $variation_type->setTraits(['purchasable_entity_shippable']);
    $variation_type->save();

    $order_type = OrderType::load('default');
    $order_type->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'pickup');
    $order_type->setThirdPartySetting('commerce_shipping', 'shipment_type', 'default');
    $order_type->save();

    // Create the order field.
    $field_definition = commerce_shipping_build_shipment_field_definition($order_type->id());
    $this->container->get('commerce.configurable_field_manager')->createField($field_definition);

    // Install the variation trait.
    $trait_manager = $this->container->get('plugin.manager.commerce_entity_trait');
    $trait = $trait_manager->createInstance('purchasable_entity_shippable');
    $trait_manager->installTrait($trait, 'commerce_product_variation', 'default');

    // Create a non-shippable product/variation type set.
    $variation_type = ProductVariationType::create([
      'id' => 'digital',
      'label' => 'Digital',
      'orderItemType' => 'default',
      'generateTitle' => TRUE,
    ]);
    $variation_type->save();

    $product_type = ProductType::create([
      'id' => 'Digital',
      'label' => 'Digital',
      'variationType' => $variation_type->id(),
    ]);
    $product_type->save();

    // Create two products. One shippable, one non-shippable.
    $variation = $this->createEntity('commerce_product_variation', [
      'type' => 'default',
      'sku' => strtolower($this->randomMachineName()),
      'price' => [
        'number' => '7.99',
        'currency_code' => 'USD',
      ],
    ]);
    /** @var \Drupal\commerce_product\Entity\ProductInterface $product */
    $this->product = $this->createEntity('commerce_product', [
      'type' => 'default',
      'title' => 'Conference hat',
      'variations' => [$variation],
      'stores' => [$this->store],
    ]);

    $order_item = $this->createEntity('commerce_order_item', [
      'type' => 'default',
      'title' => $this->randomMachineName(),
      'quantity' => 1,
      'unit_price' => new Price('7.99', 'USD'),
      'purchased_entity' => $this->product->getDefaultVariation(),
    ]);
    $order_item->save();
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $this->order = $this->createEntity('commerce_order', [
      'type' => 'default',
      'order_number' => '2020/01',
      'store_id' => $this->store,
      'uid' => $this->adminUser->id(),
      'order_items' => [$order_item],
      'state' => 'draft',
      'payment_gateway' => $payment_gateway->id(),
    ]);

    $this->container->get('plugin.manager.commerce_package_type')->clearCachedDefinitions();

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Standard shipping',
      'stores' => [$this->store->id()],
      'weight' => -10,
      'plugin' => [
        'target_plugin_id' => 'flat_rate',
        'target_plugin_configuration' => [
          'rate_label' => 'Standard shipping',
          'rate_amount' => [
            'number' => '9.99',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ]);

    $this->createEntity('commerce_shipping_method', [
      'name' => 'Standard shipping',
      'stores' => [$this->store->id()],
      // Ensure that Standard shipping shows before overnight shipping.
      'plugin' => [
        'target_plugin_id' => 'pickup',
        'target_plugin_configuration' => [
          'rate_label' => 'Pickup',
        ],
      ],
    ]);

    $this->shippingOrderManager = $this->container->get('commerce_shipping.order_manager');
  }

  /**
   * Tests pickup shipping.
   */
  public function testCheckoutPickupShipping(): void {
    $this->createEntity('profile', [
      'type' => 'customer',
      'uid' => $this->adminUser->id(),
      'address' => $this->frenchAddress,
      'is_default' => TRUE,
    ]);

    $this->drupalGet(Url::fromRoute('commerce_checkout.form', [
      'commerce_order' => $this->order->id(),
    ]));
    $this->assertSession()->waitForText('Shipping information');
    $billing_prefix = 'payment_information[billing_information]';
    $shipping_prefix = 'pickup_capable_shipping_information';
    $this->assertSession()->checkboxChecked($billing_prefix . '[copy_fields][enable]');
    $this->assertSession()->fieldExists($shipping_prefix . '[shipping_profile][select_address]');
    $this->getSession()->getPage()->selectFieldOption($shipping_prefix . '[shipments][0][shipping_method][0]', '2--pickup');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldNotExists($shipping_prefix . '[shipping_profile][address][0][address][address_line1]');
    $this->assertSession()->fieldExists($shipping_prefix . '[shipping_profile][pickup_dealer]');
    $this->getSession()->getPage()->selectFieldOption($shipping_prefix . '[shipping_profile][pickup_dealer]', 'Dealer 2');
    $this->assertSession()->pageTextContains('Payment information');
    $billing_prefix = 'payment_information[billing_information]';
    $this->assertSession()->fieldNotExists($billing_prefix . '[copy_fields][enable]');
    $this->assertSession()->fieldExists($billing_prefix . '[select_address]');
    $this->assertRenderedAddress($this->frenchAddress);
    $this->submitForm([], 'Continue to review');
    $this->assertRenderedAddress($this->dealerAddress);
    $this->clickLink('Go back');
    $this->assertSession()->waitForElement('named', ['select', $shipping_prefix . '[shipping_profile][pickup_dealer]']);
    $this->assertSession()->fieldNotExists($shipping_prefix . '[shipping_profile][address][0][address][address_line1]');
    $this->assertSession()->selectExists($shipping_prefix . '[shipping_profile][pickup_dealer]');
    $this->getSession()->getPage()->selectFieldOption($shipping_prefix . '[shipments][0][shipping_method][0]', '1--default');
    $this->assertSession()->assertWaitOnAjaxRequest();
    $this->assertSession()->fieldExists($shipping_prefix . '[shipping_profile][select_address]');
    $this->assertRenderedAddress($this->frenchAddress);
    $this->submitForm([], 'Continue to review');
    $this->assertRenderedAddress($this->frenchAddress);
  }

  /**
   * Asserts that the given address is rendered on the page.
   *
   * @param array $address
   *   The address.
   * @param string $profile_id
   *   The parent profile ID.
   */
  protected function assertRenderedAddress(array $address, $profile_id = NULL): void {
    $parent_class = $profile_id ? '.profile--' . $profile_id : '.profile';
    $page = $this->getSession()->getPage();
    $address_text = $page->find('css', $parent_class . ' p.address')->getText();
    foreach ($address as $property => $value) {
      if ($property == 'country_code') {
        $value = $this->countryList[$value];
      }
      $this->assertStringContainsString($value, (string)$address_text);
    }
  }
}
