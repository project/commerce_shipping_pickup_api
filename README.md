# Commerce Shipping Pickup API

Provides the framework for shipping methods for pickup services and parcel machines.

**This module only provides the framework. In order to use actual pickup service operators,
install at least one dependent pickup service module (usually named _Commerce Shipping Pickup
Some-Service-Name)_.**

## Installation

Install and enable the module through Composer:
```
composer require drupal/commerce_shipping_pickup_api
drush en commerce_shipping_pickup_api
```

Add the new `pickup_capable_shipping_information` pane with the label _Shipping information_
(look for the one that says _Supports pickup: Yes_ in the summary) to your checkout flow
or use the pre-built `pickup` checkout flow.

Add and set up a new shipping method (provided by dependent pickup service modules)
in your store configuration.

## Developing dependent pickup service modules

The user-friendly way is to create one module per operator,
and a module can provide more than one actual service. For easy discoverability,
the suggested name is in the format _Commerce Shipping Pickup Your-Service-Name_
(with the short, module name `commerce_shipping_pickup_yourservice`). If you have a module
with multiple services of your country's national postal company, you probably want to name
the module by country and company. If you have a separate operator with a single specific
service name, you may want to use that. At any rate, use a name that you know
the site builders will easily find.

Inside the module, you will have to give a unique identifier to each individual service
of the operator. To avoid name clashes with other modules, please, adhere to the following
format: all such identifiers have _three parts:_

* All need to start with `pickup_`, the framework requires this.

* The second part should be the country code (because operators in different countries might use
  the same service name). For global companies, use their home country.

* The actual differentiating part, created from the name of the service. If a service has
  support for different user interfaces (eg. one to select from a full list of pickup points,
  and another one to pick a point from a map), make sure you differentiate them.

For instance, a service called "ParcelPoint" in Switzerland would thus receive the name
`pickup_ch_parcelpoint`. If it also provides an embedded map to select from, that might be
called `pickup_ch_parcelpoint_map`.

### Module specification

The module has to depend on this one (`info.yml`):
```
dependencies:
  - commerce_shipping_pickup_api:commerce_shipping_pickup_api
```

### Shipping methods

You need to inform `Drupal Commerce` about your services by creating a shipping method for each.

Extend `PickupShipping` with your shipping method. This class provides the same functionality
as the usual flat rate shipping methods: a rate label, a description and an amount).
If you need your own configuration or rate calculation logic, override the appropriate functions.

`PickupShipping` implements `PickupShippingInterface` and has two abstract functions
that you **must** override (or you get an exception). `buildFormElement()` provides
the pickup point selection interface to the user and `populateProfile()` gets back
the necessary information from this interface about the user's selection and stores it
into the shipping profile in the order. Follow the documentation in the interface
to understand the connection between the inline form and the storage of the pickup point.

```
namespace Drupal\commerce_shipping_pickup_yourservice\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\ShippingMethod\PickupShipping;

/**
 * Provides a specific pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup_country_yourservice",
 *   label = @Translation("Pickup shipping - Your Service"),
 * )
 */
class PickupYourServiceShipping extends PickupShipping {
  public function buildFormElement(ProfileInterface $profile): array {
    // ...
  }

  public function populateProfile(ProfileInterface $profile): void {
    // ...
  }
}
```

If you need a radically different shipping method that has nothing to do with the
`PickupShipping` implementation, you can extend
`Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase` yourself
and create one from scratch. In this case you have to make sure that you implement
`PickupShippingInterface` and provide the two methods.

See the `commerce_shipping_pickup_demo` sub-module for a minimal sample.

### History and credits

The module was originally conceived as building upon a modified version of
[Commerce Shipping Pickup](https://www.drupal.org/project/commerce_shipping_pickup).
As that module seems to be abandoned now, it was forked as a standalone module.

Although modified to allow the addition of dependent modules, the original idea
and significant portions of the code rely on this original module.