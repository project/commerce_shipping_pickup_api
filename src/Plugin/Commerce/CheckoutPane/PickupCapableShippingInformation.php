<?php

namespace Drupal\commerce_shipping_pickup_api\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce\Plugin\Commerce\InlineForm\InlineFormInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\OrderShipmentSummaryInterface;
use Drupal\commerce_shipping\PackerManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\CheckoutPane\ShippingInformation;
use Drupal\commerce_shipping\ShipmentManagerInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the shipping information pane.
 *
 * Collects the shipping profile, then the information for each shipment.
 * Assumes that all shipments share the same shipping profile.
 *
 * @CommerceCheckoutPane(
 *   id = "pickup_capable_shipping_information",
 *   label = @Translation("Shipping information"),
 *   wrapper_element = "fieldset",
 * )
 */
final class PickupCapableShippingInformation extends ShippingInformation {
  /**
   * The shipment manager.
   *
   * @var \Drupal\commerce_shipping\ShipmentManagerInterface
   */
  protected $shipmentManager;

  /**
   * Constructs a new PickupCapableShippingInformation object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Drupal\commerce_shipping\PackerManagerInterface $packer_manager
   *   The packer manager.
   * @param \Drupal\commerce_shipping\OrderShipmentSummaryInterface $order_shipment_summary
   *   The order shipment summary.
   * @param \Drupal\commerce_shipping\ShipmentManagerInterface $shipment_manager
   *   The shipment manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfo $entity_type_bundle_info, InlineFormManager $inline_form_manager, PackerManagerInterface $packer_manager, OrderShipmentSummaryInterface $order_shipment_summary, ShipmentManagerInterface $shipment_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager, $entity_type_bundle_info, $inline_form_manager, $packer_manager, $order_shipment_summary);
    $this->shipmentManager = $shipment_manager;
  }

  /**
   * {@inheritdoc}
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new PickupCapableShippingInformation(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('commerce_shipping.packer_manager'),
      $container->get('commerce_shipping.order_shipment_summary'),
      $container->get('commerce_shipping.shipment_manager')
    );
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form): array {
    $store = $this->order->getStore();
    $available_countries = [];
    foreach ($store->get('shipping_countries') as $country_item) {
      $available_countries[] = $country_item->value;
    }

    // Default to base customer_profile inline form plugin.
    /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
    $inline_form = $this->inlineFormManager->createInstance('customer_profile', [
      'profile_scope' => 'shipping',
      'available_countries' => $available_countries,
      'address_book_uid' => $this->order->getCustomerId(),
      // Don't copy the profile to address book until the order is placed.
      'copy_on_save' => FALSE,
    ], $this->getShippingProfilePickup());

    $pane_form['shipping_profile'] = [
      '#parents' => array_merge($pane_form['#parents'], ['shipping_profile']),
      '#inline_form' => $inline_form,
    ];
    $pane_form['shipping_profile'] = $inline_form->buildInlineForm($pane_form['shipping_profile'], $form_state);
    // The shipping_profile should always exist in form state (and not just
    // after "Recalculate shipping" is clicked).
    if (!$form_state->has('shipping_profile')) {
      $form_state->set('shipping_profile', $inline_form->getEntity());
    }

    $pane_form['removed_shipments'] = [
      '#type' => 'value',
      '#value' => [],
    ];
    $pane_form['shipments'] = [
      '#type' => 'container',
      // Pickup custom: place at top.
      '#weight' => -999,
    ];

    $shipping_profile = $form_state->get('shipping_profile');
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
    $shipments = $this->order->get('shipments')->referencedEntities();
    $recalculate_shipping = $form_state->get('recalculate_shipping');
    $force_packing = empty($shipments) && $this->canCalculateRates($shipping_profile);
    if ($recalculate_shipping || $force_packing) {
      // We're still relying on the packer manager for packing the order since
      // we don't want the shipments to be saved for performance reasons.
      // The shipments are saved on pane submission.
      [$shipments, $removed_shipments] = $this->packerManager->packToShipments($this->order, $shipping_profile, $shipments);

      // Store the IDs of removed shipments for submitPaneForm().
      $pane_form['removed_shipments']['#value'] = array_map(function ($shipment) {
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
        return $shipment->id();
      }, $removed_shipments);
    }
    $default_rate = NULL;
    $single_shipment = count($shipments) === 1;
    foreach ($shipments as $index => $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $pane_form['shipments'][$index] = [
        '#parents' => array_merge($pane_form['#parents'], ['shipments', $index]),
        '#array_parents' => array_merge($pane_form['#parents'], ['shipments', $index]),
        '#type' => $single_shipment ? 'container' : 'fieldset',
        '#title' => $shipment->getTitle(),
      ];
      $form_display = EntityFormDisplay::collectRenderDisplay($shipment, 'checkout');
      $form_display->removeComponent('shipping_profile');
      $form_display->buildForm($shipment, $pane_form['shipments'][$index], $form_state);
      $pane_form['shipments'][$index]['#shipment'] = $shipment;

      // Pickup custom: Add ajax.
      $widget = &$pane_form['shipments'][$index]['shipping_method']['widget'][0];

      $widget['#ajax'] = [
        'callback' => [self::class, 'ajaxRefreshForm'],
        'element' => $widget['#field_parents'],
      ];
      $widget['#limit_validation_errors'] = [
        $widget['#field_parents'],
      ];
      $rates = $this->shipmentManager->calculateRates($shipment);
      $default_rate = $this->shipmentManager->selectDefaultRate($shipment, $rates);
    }

    // Replace the inline form with our own if a pickup shipping method is
    // selected or default.
    if (self::isPickupSelected($pane_form, $form_state, $this->order, $default_rate)) {
      $inline_form = $this->inlineFormManager->createInstance('pickup_profile', [
        'profile_scope' => 'shipping',
        'available_countries' => $available_countries,
      ], $this->getShippingProfilePickup(TRUE));

      assert($inline_form instanceof InlineFormInterface);

      $pane_form_shipping_profile = [
        '#parents' => array_merge($pane_form['#parents'], ['shipping_profile']),
        '#inline_form' => $inline_form,
      ];
      $pane_form['shipping_profile'] = $inline_form->buildInlineForm($pane_form_shipping_profile, $form_state);
      // We don't want the copy fields feature on the billing inline form.
      $form_state->set('shipping_profile', NULL);
    }
    return $pane_form;
  }

  /**
   * Determines if a pickup shipping method is selected.
   *
   * This function is static, to not duplicate code.
   * Also used in ProfileFieldCopyWithoutPickup.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   * @param \Drupal\commerce_shipping\ShippingRate|NULL $default_rate
   *   The default shipping rate;
   *
   * @return bool
   *   A pickup is selected or not.
   */
  public static function isPickupSelected(array $form, FormStateInterface $form_state, OrderInterface $order, ShippingRate $default_rate = NULL): bool {
    $result = FALSE;
    $shipping_method = NestedArray::getValue(
      $form_state->getUserInput(),
      array_merge($form['#parents'], ['shipments', 0, 'shipping_method', 0])
    );
    if (!empty($shipping_method)) {
      $result = strpos($shipping_method, 'pickup') !== FALSE;
    } else {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
      $shipments = $order->get('shipments')->referencedEntities();
      $shipment = reset($shipments);
      if ($shipment !== FALSE) {
        $result = strpos($shipment->getShippingMethod()->getPlugin()->getPluginId(), 'pickup') !== FALSE;
      } else {
        // Fallback on the default shipping rate.
        if ($default_rate instanceof ShippingRate) {
          $result = strpos($default_rate->getId(), 'pickup') !== FALSE;
        }
      }
    }
    return $result;
  }

  /**
   * Gets the shipping profile and clears address data on switch.
   *
   * @param bool $isPickup
   *   Is pickup store profile?
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The retrieved profile.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getShippingProfilePickup(bool $isPickup = FALSE): ProfileInterface {
    $profile = $this->getShippingProfile();

    $profile_data = $profile->getData('pickup_location_data', FALSE);
    $not_pickup_has_data = !$isPickup && $profile_data !== FALSE;
    $is_pickup_no_data = $isPickup && $profile_data === FALSE;
    if ($not_pickup_has_data || $is_pickup_no_data) {
      $profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => $profile->bundle(),
        'uid' => 0,
      ]);
    }

    return $profile;
  }

  /**
   * Returns the current order.
   *
   * @return OrderInterface
   */
  public function getOrder(): OrderInterface {
    return $this->order;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    unset($form['require_shipping_profile']);
    unset($form['auto_recalculate']);

    $form['support_pickup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Supports pickup'),
      '#default_value' => TRUE,
      '#disabled' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $this->configuration['require_shipping_profile'] = FALSE;
      $this->configuration['auto_recalculate'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary(): string {
    return $this->t('Supports pickup: Yes')->render();
  }
}
