<?php

namespace Drupal\commerce_shipping_pickup_api\Plugin\Commerce\InlineForm;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\MultistepDefault;
use Drupal\commerce_shipping_pickup_api\PickupShippingInterface;
use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\CheckoutPane\PickupCapableShippingInformation;
use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides an inline form for managing a pickup profile.
 *
 * @CommerceInlineForm(
 *   id = "pickup_profile",
 *   label = @Translation("Pickup profile"),
 * )
 */
final class PickupProfile extends EntityInlineFormBase {

  /**
   * The pickup shipping method.
   *
   * @var \Drupal\commerce_shipping_pickup_api\PickupShippingInterface
   */
  protected $shippingMethod;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      // Unique. Passed along to field widgets. Examples: 'billing', 'shipping'.
      'profile_scope' => 'shipping',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $this->shippingMethod = NULL;
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    // Follow the chain: form > pane > order > shipment > shipping method
    /** @var MultistepDefault $form_object */
    $form_object = $form_state->getFormObject();
    /** @var PickupCapableShippingInformation $pane */
    $pane = $form_object->getPane('pickup_capable_shipping_information');
    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface[] $shipments */
    $shipments = $pane->getOrder()->get('shipments')->referencedEntities();
    if (count($shipments) > 0) {
      $shipment = reset($shipments);
      $this->shippingMethod = $shipment->getShippingMethod()->getPlugin();
      assert($this->shippingMethod instanceof PickupShippingInterface);
      /** @var ProfileInterface $profile */
      $profile = $this->entity;
      $inline_form['pickup_dealer'] = $this->shippingMethod->buildFormElement($profile);
    }

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);

    if ($this->shippingMethod != NULL) {
      assert($this->shippingMethod instanceof PickupShippingInterface);

      /** @var ProfileInterface $profile */
      $profile = $this->entity;

      $values = NestedArray::getValue($form_state->getValues(), $inline_form['#parents']);
      $profile->setData('pickup_location_data', $values['pickup_dealer']);
      // Do not save a dealer pickup address into the users address book.
      $profile->unsetData('address_book_profile_id');
      $profile->unsetData('copy_to_address_book');
      $this->shippingMethod->populateProfile($profile);
      $profile->save();
    } else {
      \Drupal::messenger()->addError('Error in inline form.');
    }
  }
}
