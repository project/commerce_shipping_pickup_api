<?php

namespace Drupal\commerce_shipping_pickup_api;

use Drupal\commerce_shipping\ProfileFieldCopy;
use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\CheckoutPane\PickupCapableShippingInformation;
use Drupal\Core\Form\FormStateInterface;

/**
 * If a pickup method is selected, disable "billing same as shipping".
 */
final class ProfileFieldCopyWithoutPickup extends ProfileFieldCopy {

  /**
   * {@inheritdoc}
   */
  public function supportsForm(array &$inline_form, FormStateInterface $form_state): bool {
    $parent = parent::supportsForm($inline_form, $form_state);

    if (empty($form_state->getCompleteForm()['pickup_capable_shipping_information'])) {
      return $parent;
    }
    $order = self::getOrder($form_state);
    $form = $form_state->getCompleteForm()['pickup_capable_shipping_information'];
    if (PickupCapableShippingInformation::isPickupSelected($form, $form_state, $order)) {
      return FALSE;
    }

    return $parent;
  }
}
