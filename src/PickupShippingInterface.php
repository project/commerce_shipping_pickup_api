<?php

namespace Drupal\commerce_shipping_pickup_api;

use Drupal\profile\Entity\ProfileInterface;

/**
 * Interface of the pickup shipping method.
 */
interface PickupShippingInterface {

  /**
   * Defines the type of input field(s) the pane should provide.
   *
   * List case: if your service provides a full list of pickup points indexed by a unique identifier,
   * you can simply use a `select` so that the user can choose one from the list.
   * The selection will be stored into `pickup_location_data` in the shipping profile.
   *
   * ```
   * public function buildFormElement(ProfileInterface $profile): array {
   *   return [
   *     '#type' => 'select',
   *     '#title' => $this->t('Select a pickup point:'),
   *     '#default_value' => $profile->getData('pickup_location_data'),
   *     '#options' => array_map(function ($address) {
   *       return $address['organization'];
   *     }, $this->dealers),
   *   ];
   * }
   * ```
   *
   * Online selector case: some services have no fixed list but an online selector to be embedded.
   * When the user selects a point, the selector will inform us about the selection.
   * In this case, you can use `textfield` and `hidden` elements to store the details of the selected pickup point.
   * These details will be stored into `pickup_location_data` in the shipping profile.
   *
   * ```
   * public function buildFormElement(ProfileInterface $profile): array {
   *   $element = [
   *     '#type' => 'container',
   *   ];
   *   $element['selector'] = [
   *     '#type' => 'inline_template',
   *     '#template' => '<iframe width="100%" height="500px" src="{{ url }}"></iframe>',
   *     '#context' => ['url' => 'https://...'],
   *   ];
   *   $element['id'] = [
   *     '#type' => 'hidden',
   *     '#required' => TRUE,
   *   ];
   *   $element['organization'] = [
   *     '#type' => 'textfield',
   *     '#title' => $this->t('Name'),
   *     '#attributes' => ['readonly' => 'readonly'],
   *     '#required' => TRUE,
   *   ];
   *   $element['address_line1'] = [
   *     '#type' => 'textfield',
   *     '#title' => $this->t('Address'),
   *     '#attributes' => ['readonly' => 'readonly'],
   *     '#required' => TRUE,
   *   ];
   *
   *   return $element;
   * }
   * ```
   *
   * @param \Drupal\profile\ProfileInterface $profile
   *   The profile as context.
   */
  public function buildFormElement(ProfileInterface $profile): array;

  /**
   * Populates the profile with data of the selected pickup point.
   *
   * `populateProfile()` retrieves what [buildFormElement()] stored when the user selected
   * the desired pickup point. In the list case mentioned there, this data was the ID
   * of the point, so we use it to look up other details of the point from a `$dealers`
   * list containing data of all pickup points:
   *
   * ```
   * $id = $profile->getData('pickup_location_data');
   * $data = $this->dealers[$id];
   * ```
   *
   * In the online selector case, both the ID and other details of the point
   * were stored directly as an array:
   *
   * ```
   * $data = $profile->getData('pickup_location_data');
   * ```
   *
   * So, we can put all details into the shipping profile (don't forget the country code, it's required
   * for a Commerce `address` to function correctly, the rest is up to you):
   *
   * ```
   * $profile->set('address', [
   *   "country_code" => "CH",
   *   "organization" => $data['organization'],
   *   "address_line1" => $data['address_line1'],
   * ]);
   * ```
   *
   * @param \Drupal\profile\ProfileInterface $profile
   *   The profile to populate.
   */
  public function populateProfile(ProfileInterface $profile): void;
}
